from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from django.utils import timezone
from django.contrib import messages
from datetime import datetime

from .models import Matkul, Tugas
from .forms import MatkulForm, TugasForm


# Create your views here.
def matkul(request):
    matkul = Matkul.objects.all()

    return render(request, 'story5/matkul.html', {'matkul':matkul})

def details(request, id): 
    matkul = Matkul.objects.get(id=id)
    tugas = Tugas.objects.all().filter(matkul=matkul)

    form = TugasForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = TugasForm() 

    context = {
        'matkul':matkul, 
        'del':False, 
        'tugas':tugas, 
        'form':form,
    }    

    # tugas and deadlines
    tugasDL = []
    for tugas in context['tugas']:
        due = tugas.dl - timezone.now()
        tugasDL.append((tugas.nama, due.total_seconds(), tugas.dl))
    context['deadlines'] = tugasDL           

    return render(request, 'story5/details.html', context)

def addMatkul(request): 
    form = MatkulForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = MatkulForm()

    return render(request, 'story5/addMatkul.html', {'form':form})
 
def delMatkul(request, id):
    matkul = get_object_or_404(Matkul, id=id)

    if request.method == 'POST':
        matkul.delete()
        messages.success(request, 'Successfully deleted Mata Kuliah {}'.format(matkul.nama))
        return redirect('../')
    
    return render(request, 'story5/details.html', {'matkul':matkul, 'del':True})