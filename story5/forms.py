from django.forms import ModelForm, DateTimeInput, DateTimeField
from django.utils.translation import gettext_lazy as _

from .models import Matkul, Tugas

class MatkulForm(ModelForm): 
    class Meta: 
        model = Matkul
        fields = '__all__'

class TugasForm(ModelForm):
    class Meta: 
        model = Tugas
        fields = '__all__'
        labels = {
            'nama': _('Nama tugas'),
            'dl': _('Deadline'),
        }
        widgets = {
            'dl': DateTimeInput(attrs={'class': 'datetimepicker'}),
        }