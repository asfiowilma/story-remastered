from django.db import models
from django.utils import timezone
from datetime import date

# Create your models here.
class Matkul(models.Model):
    nama = models.CharField(max_length=50)
    dosen = models.CharField(max_length=50)
    sks = models.IntegerField()
    desc = models.TextField()
    sem = models.CharField(max_length=20)
    ruang = models.CharField(max_length=20)

    def __str__ (self):
        return self.nama

    class Meta: 
        verbose_name_plural = "Matkul"

class Tugas(models.Model): 
    nama = models.CharField(max_length=50)
    dl = models.DateTimeField()
    matkul = models.ForeignKey(Matkul, default=1, on_delete=models.CASCADE)

    def __str__ (self):
        return self.nama + " in " + self.matkul.nama

    class Meta:
        ordering = ["dl"]
        verbose_name_plural = "Tugas"