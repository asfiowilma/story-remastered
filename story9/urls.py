from django.urls import path
from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.index, name='index'),
    path('like/', views.like, name='like'),
    path('top5/', views.top5, name='top5')
]