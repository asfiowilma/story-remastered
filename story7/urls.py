from django.urls import path
from . import views

app_name = 'story7'

urlpatterns = [
    path('', views.index, name='index'),
    path('confirm/<int:confirm>', views.confirm, name='confirm'),
    path('color/<int:id>', views.color, name='color')
]
