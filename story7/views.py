from django.shortcuts import render, redirect, reverse
from django.urls import resolve, reverse
from django.contrib import messages

import random

from .forms import Form
from .models import StatusMessage

# Create your views here.
def index(request):  
    status_msg = StatusMessage.objects.all()
    new_msg = Form()

    color_choice = ['burlywood', 'bisque', 'peachpuff', 'rosybrown']
    choice = color_choice[random.randint(0,3)]

    context = {
        'status_msg': status_msg,
        'new_msg': new_msg,
        'choice': choice
    }
    return render(request, 'story7/index.html', context)

def confirm(request, confirm):
    # confirm == 0 (TO BE CONFIRMED)
    if request.method == 'POST':
        name = request.POST['name']
        msg = request.POST['msg']
        
        new = StatusMessage(name = name, msg = msg)
        if confirm == 0:             
            return render(request, 'story7/confirm.html', {'new': new})    
        elif confirm == 1: 
            # saves new status to database and post it on the board
            new.save()
            messages.add_message(request, messages.SUCCESS, "Your message is posted.")
            return redirect(reverse('story7:index'))      
        elif confirm == 2: 
            # cancel posting 
            messages.add_message(request, messages.SUCCESS, "Your post is canceled.")
            return redirect(reverse('story7:index'))
    return render(request, 'story7/confirm.html')

def color(request, id):
    box = StatusMessage.objects.get(id=id)
    color = request.POST.get("color", "")

    newbox = StatusMessage(id=id, name=box.name, msg= box.msg, color=color)
    newbox.save()
    return redirect(reverse('story7:index'))
