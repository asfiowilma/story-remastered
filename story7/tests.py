from django.test import TestCase
from django.urls import resolve, reverse
from django.http import HttpRequest
from .views import index, confirm, color
from .models import StatusMessage

# Create your tests here.
class HomePageTest(TestCase):

    def test_root_url_resolves_to_index_view(self):
        found = resolve(reverse('story7:index'))
        self.assertEqual(found.func, index)

    def test_color_url_resolves_to_index_view(self):
        found = resolve(reverse('story7:color', args=[0]))
        self.assertEqual(found.func, color)
    
    def test_home_page_returns_correct_html(self):
        response = self.client.get(reverse('story7:index'))
        html = response.content.decode('utf8')
        self.assertIn('PacilBoard', html)
        self.assertTemplateUsed(response, 'story7/index.html')

    def test_only_saves_status_msg_when_necessary(self): 
        self.client.get(reverse('story7:index'))
        self.assertEqual(StatusMessage.objects.count(),0)

    def test_redirects_after_POST(self):
        response = self.client.post(reverse('story7:confirm', args=[0]), data={
            'msg': 'hewwo world',
            'name': 'Dek Depe'
        })
        self.assertContains(response, "Are you sure", 1, 200)

class ConfirmationPageTest(TestCase):
    
    def test_confirm_page_url_resolves_to_confirm_view(self):
        found = resolve(reverse('story7:confirm', args=[0]))
        self.assertEqual(found.func, confirm)

    def test_confirm_page_returns_correct_html(self):
        response = self.client.get(reverse('story7:confirm', args=[0]))
        html = response.content.decode('utf8')
        self.assertIn('Confirm', html)
        self.assertTemplateUsed(response, 'story7/confirm.html')

    def test_displays_recent_status_to_confirm(self):
        response = self.client.post(reverse('story7:confirm', args=[0]), data={
            'msg': 'hewwo world',
            'name': 'Dek Depe'
        })
        # one displayed, two in hidden form fields
        self.assertContains(response, 'Dek Depe', 3, 200)
        self.assertContains(response, 'hewwo world', 3, 200)

    def test_confirm_save_recent_status(self): 
        response = self.client.post(reverse('story7:confirm', args=[1]), data={
            'msg': 'hewwo world',
            'name': 'Dek Depe'
        })
        saved_msgs = StatusMessage.objects.all()
        self.assertEqual(saved_msgs.count(), 1)


    def test_confirm_cancel_recent_status(self): 
        response = self.client.post(reverse('story7:confirm', args=[2]), data={
            'msg': 'hewwo world',
            'name': 'Dek Depe'
        })
        saved_msgs = StatusMessage.objects.all()
        self.assertEqual(saved_msgs.count(), 0)
        

class StatusMessageModelTest(TestCase):

    def test_saving_and_retrieving_status_message(self):
        msg1 = StatusMessage(name="Person1", msg = 'This is my first status message')
        msg1.save()

        msg2 = StatusMessage(name='Person2', msg = 'This is a second one')
        msg2.save()

        saved_msgs = StatusMessage.objects.all()
        self.assertEqual(saved_msgs.count(), 2)

        first_saved_msg = saved_msgs[0]
        second_saved_msg = saved_msgs[1]
        self.assertEqual(first_saved_msg.msg, 'This is my first status message')
        self.assertEqual(second_saved_msg.msg, 'This is a second one')
        self.assertEqual(str(first_saved_msg), 'Status message by Person1')
    
    def test_change_color_after_POST(self):
        msg1 = StatusMessage(name="Person1", msg = 'This is my first status message')
        msg1.save()

        msg2 = StatusMessage(name='Person2', msg = 'This is a second one')
        msg2.save()        

        response = self.client.post(reverse('story7:color', args=[1]), data={
            'color': 'peachpuff'
        })

        saved_msgs = StatusMessage.objects.all()
        second_saved_msg = saved_msgs[0]
        self.assertEqual(second_saved_msg.color, 'peachpuff')