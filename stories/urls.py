"""stories URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from . import views 

import story1.urls as story1
import story4.urls as story4
import story5.urls as story5
import story6.urls as story6
import story7.urls as story7
import story8.urls as story8
import story9.urls as story9
import story10.urls as story10

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name="index"),
    path('story1/', include(story1, namespace='story1')),
    path('story4/', include(story4, namespace='story4')),
    path('story5/', include(story5, namespace='story5')),
    path('story6/', include(story6, namespace='story6')),
    path('story7/', include(story7, namespace="story7")),
    path('story8/', include(story8, namespace="story8")),
    path('story9/', include(story9, namespace="story9")),
    path('story10/', include(story10, namespace="story10"))
]
