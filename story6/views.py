from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.contrib import messages
from .models import Kegiatan, Peserta
from .forms import PesertaForm, KegiatanForm

# Create your views here.
def index(request):  
    kegiatan = Kegiatan.objects.all()
    add_peserta = PesertaForm(request.POST or None)
    add_kegiatan = KegiatanForm(request.POST or None)
    if add_peserta.is_valid():
        add_peserta.save()
        add_peserta = PesertaForm()
    
    if add_kegiatan.is_valid():
        add_kegiatan.save()
        add_kegiatan = KegiatanForm()

    count = 3
    for i in kegiatan: 
        count += 1
    masonry_height = count // 3 * 500

    context = {
        'kegiatan': kegiatan,
        'add_peserta': add_peserta,
        'add_kegiatan': add_kegiatan,
        'height': masonry_height
    }
    return render(request, 'story6/index.html', context)

def detail(request, nama): 
    nama = nama.replace("-"," ")
    kegiatan = Kegiatan.objects.get(nama=nama)
    add_peserta = PesertaForm(request.POST or None)
    if add_peserta.is_valid():
        add_peserta.save()
        add_peserta = PesertaForm()

    context = {
        'kegiatan': kegiatan,
        'add_peserta': add_peserta
    }
    return render(request, 'story6/detail.html', context)

def addKegiatan(request, id, origin):
    if request.method == 'POST':
        if origin == 'index':
            kegiatan = request.POST['nama']

            new, status = Kegiatan.objects.get_or_create(nama=kegiatan)
            if status: 
                # kegiatan belum pernah ada
                new.save()
                messages.add_message(request, messages.SUCCESS, "{} is successfully created.".format(new))
            else: 
                # kegiatan sudah ada
                messages.add_message(request, messages.WARNING, "{} already exists.".format(new))
                
            return redirect(reverse('story6:detail', args=[new.nama]))
        
        elif origin == 'detailPeserta':
            peserta = Peserta.objects.get(id=id)
            kegiatan = request.POST['nama']

            new, status = Kegiatan.objects.get_or_create(nama=kegiatan)
            if status: 
                # add peserta baru ke kegiatan
                new.save()
                peserta.kegiatan_set.add(new)
                messages.add_message(request, messages.SUCCESS, "{} is now participating in {}".format(peserta, new))
            else: 
                try: 
                    # peserta lama udah ada di kegiatan
                    peserta.kegiatan_set.get(nama=new)
                    messages.add_message(request, messages.WARNING, "{} was already participating in {}".format(peserta, new))
                except: 
                    # peserta lama belom ada di kegiatan
                    peserta.kegiatan_set.add(new)
                    messages.add_message(request, messages.SUCCESS, "{} is now participating in {}".format(peserta, new))
            
            return redirect(reverse('story6:detailPeserta', args=[peserta.nama]))

def detailPeserta(request, nama): 
    nama = nama.replace("-"," ")
    peserta = Peserta.objects.get(nama=nama)
    add_kegiatan = KegiatanForm(request.POST or None)
    if add_kegiatan.is_valid():
        add_kegiatan.save()
        add_kegiatan = KegiatanForm()

    context = {
        'peserta': peserta,
        'add_kegiatan': add_kegiatan
    }
    return render(request, 'story6/detailPeserta.html', context)

def addPeserta(request, id, origin): 
    if request.method == 'POST':
        kegiatan = Kegiatan.objects.get(id=id)
        peserta = request.POST['nama']

        new, status = Peserta.objects.get_or_create(nama=peserta)
        if status: 
            # add peserta baru ke kegiatan
            new.save()
            kegiatan.peserta.add(new)
            messages.add_message(request, messages.SUCCESS, "{} was successfully added to {}".format(new, kegiatan.nama))
        else: 
            try: 
                # peserta lama udah ada di kegiatan
                kegiatan.peserta.get(nama=new)
                messages.add_message(request, messages.WARNING, "{} was already participating in {}".format(new, kegiatan.nama))
            except: 
                # peserta lama belom ada di kegiatan
                kegiatan.peserta.add(new)
                messages.add_message(request, messages.SUCCESS, "{} was successfully added to {}".format(new, kegiatan.nama))
    
    if origin == 'index': 
        return redirect(reverse('story6:index'))
    elif origin == 'detail': 
        return redirect(reverse('story6:detail', args=[kegiatan.nama]))

def delPeserta(request, id, pk, origin): 
    kegiatan = Kegiatan.objects.get(id=id)

    try: 
        delPeserta = get_object_or_404(Peserta, pk=pk)
        kegiatan.peserta.remove(delPeserta)
        messages.add_message(request, messages.SUCCESS, "{} successfully removed".format(delPeserta))
    except: 
        messages.add_message(request, messages.SUCCESS, "{} didn't exist".format(delPeserta))
           
    if origin == 'index': 
        return redirect(reverse('story6:index'))
    elif origin == 'detail': 
        return redirect(reverse('story6:detail', args=[kegiatan.nama]))
    


