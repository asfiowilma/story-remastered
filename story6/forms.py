from django.forms import ModelForm
from django import forms
from django.utils.translation import gettext_lazy as _
from .models import Peserta, Kegiatan

class PesertaForm(ModelForm): 
    class Meta: 
        model = Peserta
        fields = '__all__'
        labels = {
            'nama': _('Add Peserta'),
        }
        widgets = {
            'nama': forms.TextInput(
				attrs={
					'class': 'form-control',
                    'placeholder': 'Nama Peserta',
					}
				)
        }

class KegiatanForm(ModelForm): 
    class Meta: 
        model = Kegiatan
        fields = ('nama',)
        labels = {
            'nama': _(''),
        }        
        widgets = {
            'nama': forms.TextInput(
				attrs={
					'class': 'form-control',
                    'placeholder': 'Event name',
					}
				)
        }