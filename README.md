[![pipeline status](https://gitlab.com/asfiowilma/pacilboard/badges/master/pipeline.svg)](https://gitlab.com/asfiowilma/pacilboard/-/commits/master)
[![coverage report](https://gitlab.com/asfiowilma/pacilboard/badges/master/coverage.svg)](https://gitlab.com/asfiowilma/pacilboard/-/commits/master)

Repository for Web Design and Programming Stories 7 through 8 
*  Story7 - PacilBoard. A status-message-displaying webpage ([view](http://story7-pacilboard.herokuapp.com/))
*  Story8 - Profile. Accordion boxes with jQuery ([view](http://story7-pacilboard.herokuapp.com/story8))
*  Story9 - RakBooKoo. Discover books and leave a like, with ajax and GoogleBooksAPI ([view](http://story7-pacilboard.herokuapp.com/story9))
*  Story10 - User Authentication. Login and register feature using Django Auth ([view](http://story7-pacilboard.herokuapp.com/story10))