from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.CharField(max_length=1000, default="https://www.pngitem.com/pimgs/m/129-1290719_drawn-face-meme-funny-meme-face-png-transparent.png")
    fname = models.CharField(max_length=20, default="")
    lname = models.CharField(max_length=20, default="")

    def __str__(self):
        return f"{self.user.username}'s Profile"

@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs): 
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_profile(sender, instance, created, **kwargs): 
    instance.profile.save()