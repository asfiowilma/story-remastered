from django import forms
from crispy_forms.helper import FormHelper

from .models import Profile

class EditProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = '__all__'
        exclude = ['user']
        widgets = {
            'image': forms.TextInput(
				attrs={
                    'class': 'form-control',
                    'id': 'image',
                    'placeholder': 'Paste an image URL',
					}
				),
            'fname': forms.TextInput(
				attrs={
                    'class': 'form-control',
                    'id': 'fname',
                    'placeholder': 'Enter your first name',
					}
				),
            'lname': forms.TextInput(
				attrs={
                    'class': 'form-control',
                    'id': 'lname',
                    'placeholder': 'Enter your last name',
					}
				),
        }
    
    def __init__(self, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.fields['image'].label = "New profile picture"
        self.fields['fname'].label = "First Name"
        self.fields['lname'].label = "Last Name"