from django.urls import path
from django.contrib.auth import views as auth 
from . import views

app_name = 'story10'

urlpatterns = [
    path('', views.index, name='index'),
    path('signup/', views.signup, name='signup'),
    path('login/', auth.LoginView.as_view(template_name='story10/login.html'), name='login'),
    path('logout/', auth.LogoutView.as_view(template_name='story10/logout.html'), name='logout'),
    path('user/', views.user, name='user'),
]