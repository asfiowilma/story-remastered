from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait

import time

class Story7(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser =  webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
    
    def tearDown(self): 
        self.browser.quit()

    def check_for_status_msg_in_index_page(self, msg):
        board = self.browser.find_element_by_class_name('card-columns')
        cards = board.find_elements_by_class_name('message')
        self.assertIn(msg, [card.text for card in cards])

    def test_can_open_index_page(self): 
        # Kak Pewe opens the new PacilBoard website's homepage
        self.browser.get(self.live_server_url)
        self.browser.implicitly_wait(10)

        # He notices the page title and header mention PacilBoard        
        self.assertIn('PacilBoard', self.browser.title)

        # Kak Pewe sees a form to insert his name
        # and to enter a new status message
        form_name = self.browser.find_element_by_id('form_name')
        self.assertEqual(
            form_name.get_attribute('placeholder'),
            'Enter your name'
        )

        form_message = self.browser.find_element_by_id('form_message')
        self.assertEqual(
            form_message.get_attribute('placeholder'),
            'What\'s on your mind?'
        )

        # He types his own name and a new message 
        # "Hewwo Wowld" into the text box
        form_name.send_keys('Kak Pewe')
        form_message.send_keys('Hewwo Wowld')

        # When he hits enter, he's redirected to a confirmation page 
        form_name.send_keys(Keys.ENTER)
        self.browser.implicitly_wait(10)  
        self.assertIn('Confirm', self.browser.title)

        # Kak Pewe decides he would post this new message to the webpage and clicks YES.
        yes = self.browser.find_element_by_id('yes')
        yes.click()

        # Kak Pewe gets redirected back to the homepage 
        # which now shows his new status message on display 
        self.browser.implicitly_wait(10)
        self.check_for_status_msg_in_index_page('Hewwo Wowld')

        # Apparently you can change the message box's color now
        # Kak pewe clicks the button provided
        change = self.browser.find_element_by_class_name('color-button')
        change.click()

        box = self.browser.find_element_by_class_name('card')
        # Now his message box changes colors! Amazing.
        self.assertNotIn(
            'background: antiquewhite;',
            box.get_attribute('style')            
        )

class Story8(LiveServerTestCase): 
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser =  webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
            
    def tearDown(self): 
        self.browser.quit()

    def test_can_open_index_page(self): 

        # Nathan wonders why his author isn't participating in camp nanowrimo this month 
        # Turns out she's busy building wep pages for her class assignments 
        # So Nathan decides to check it out
        self.browser.get('%s%s' % (self.live_server_url, '/story8/'))
        self.browser.implicitly_wait(10)

        # The webpage shows a profile of some sort
        # He sees a familiar logo in the middle of the page
        logo = self.browser.find_element_by_id('logo')
        self.assertEqual(
            logo.get_attribute('alt'),
            'logo'
        )

        # Below that is the actual profile itself 
        # It's shown in accordion type of boxes 
        # Nathan clicks one of them 
        acc = self.browser.find_element_by_class_name('target1')
        acc_header = acc.find_element_by_class_name('acc-header')
        acc_header.click()

        # It shows the current activities of his writer
        # He now understands why she couldn't participate in camp nanowrimo this month :( 
        activities = acc.find_element_by_class_name('acc-header-wrapper')
        self.assertIn(
            'active',
            activities.get_attribute('class')            
        )
            
        # He clicks another one just to be sure         
        acc1 = self.browser.find_element_by_class_name('target4') 
        acc1_header = acc1.find_element_by_class_name('acc-header') 
        time.sleep(.1)        
        acc1_header.click()

        # This time it shows her hobbies and interests
        # At the same time, the activities box closes up
        hobbies = acc1.find_element_by_class_name('acc-header-wrapper')
        time.sleep(2)  
        self.assertIn(
            'active',
            hobbies.get_attribute('class')            
        )

        # Nathan sees an up and down arrow on each of the accordion header 
        # Wondering what it does, he clicks down on the very first accordion header 
        position = self.browser.find_elements_by_class_name("acc-item")

        self.assertEqual(acc, position[0])
        self.assertNotEqual(acc1, position[0])
        self.assertEqual(acc1, position[3])

        down = self.browser.find_elements_by_class_name("move-down")
        down[0].click()

        # Magically, the first header now switches places with the one below it
        position = self.browser.find_elements_by_class_name("acc-item")
        self.assertEqual(acc, position[1])

        # Amazed by this phenomenon, Nathan clicks down on the same header once again
        down[0].click()

        # And now it switches places again with the one below it
        # The header that was on the very top now becomes the third header on the page
        position = self.browser.find_elements_by_class_name("acc-item")
        print(position)
        self.assertEqual(acc, position[2])

        # Nathan felt bad for reordering the page as he liked 
        # So he clicked up on the same header to try and reposition it back to where it was 
        up = self.browser.find_elements_by_class_name("move-up")
        up[2].click()

        position = self.browser.find_elements_by_class_name("acc-item")
        self.assertEqual(acc, position[1])

        # He does it one more time and now the order is correct again
        up[2].click()

        position = self.browser.find_elements_by_class_name("acc-item")
        self.assertEqual(acc, position[0])

        # CHALLENGE STORY8 
        # Nathan hears that he could change the page's color scheme 
        # with a single click of a button. 
        body = self.browser.find_element_by_tag_name("html")
        self.assertIn(
            'dark',
            body.get_attribute('class')            
        )
        theme = self.browser.find_element_by_class_name("theme-toggle")
        
        # He tries it 
        theme.click()
        time.sleep(1)       

        # Now the page inverses its colors. Wow! 
        self.assertNotIn(
            'dark',
            body.get_attribute('class')            
        )        
        self.assertIn(
            'light',
            body.get_attribute('class')            
        )

        # Nathan leaves the page and sincerely hopes that his writer will participate 
        # on July's camp nanowrimo and wont neglect him for too long :(

class Story9(LiveServerTestCase): 
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser =  webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
            
    def tearDown(self): 
        self.browser.quit()

    def test_can_open_index_page(self): 
        # Nikolai doesn't like to read books because apparently he has so much work to do
        # But with the quarantine going on, he decides that he should start reading something
        # He heard about this website which shows top books recommended by his friends
        self.browser.get(self.live_server_url + '/story9/')
        self.browser.implicitly_wait(10)

        # He sees the page title upfront 
        self.assertIn('RakBooKoo', self.browser.title)

        # Nikolai notices a search box on the page
        search = self.browser.find_element_by_id('search')
        searchbut = self.browser.find_element_by_id('searchbutton')
        self.assertEqual(
            search.get_attribute('placeholder'),
            'Find interesting bookoos~'
        )

        # He types something he finds interesting,
        # "A Study in Scarlet" into the text box and hits enter
        search.send_keys('A Study')
        searchbut.click()

        # Nikolai sees ten search results and recognizes one of it 
        # He hits like on the book he recognizes
        time.sleep(5)
        like = self.browser.find_element_by_class_name('likebutton')
        like.click()

        # Satisfied, he clicks it one more time 
        like.click()

        # He types in more book title to find
        # "Finale" into the text box and hits enter
        search.send_keys('in Scarlet')
        searchbut.click()
         
        # He hits like on the book he recognizes
        like = self.browser.find_element_by_class_name('likebutton')
        for i in range (10):
            like.click()
            time.sleep(.5)

        # Nikolai scrolls up and see an intriguing button 
        # It says Top 5 without any further explanation 
        top5 = self.browser.find_element_by_id('top5button')
        self.assertIn(
            'btn',
            top5.get_attribute('class')            
        )
        top5.click()

        # He sees a modal pops up
        # It shows the two books he liked earlier
        # Nikolai thinks it should've shown five books, but he's satisfied with what he sees 
        time.sleep(1)
        modal = self.browser.find_element_by_id('top5content')
        totalLikes = modal.find_element_by_class_name('badge')
        self.assertEqual('12', totalLikes.text)

        # He closes the page with a book in mind 
        # Maybe it's time to start reading after all 


class Story10(LiveServerTestCase): 
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser =  webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
            
    def tearDown(self): 
        self.browser.quit()

    def test_can_login(self): 
        # Damian's got nothing to do, so he opens up this new web page 
        # He clicked a button that leads to the signup page 
        self.browser.get(self.live_server_url + '/story10')
        self.browser.implicitly_wait(10)

        self.assertIn('That\'s All I Ask of You', self.browser.title)
        signupButton = self.browser.find_element_by_id('signupButton')
        self.assertEqual(
            signupButton.get_attribute('type'),
            'button'
        )
        signupButton.click()
        
        # Damian went to the signup page for fun 
        # He sees a field to enter his name and a new password, plus a signup button
        header = self.browser.find_element_by_class_name('header')
        self.assertEqual('Sign Up', header.text)

        unameField = self.browser.find_element_by_id('id_username')
        pass1Field = self.browser.find_element_by_id('id_password1')
        pass2Field = self.browser.find_element_by_id('id_password2')

        self.assertEqual(
            unameField.get_attribute('name'),
            'username'
        )

        # But instead of entering his name, he enters somebody else's 
        # Damian also made up a password on the spot 
        unameField.send_keys('Putri_Badai')
        pass1Field.send_keys('iceprincess47')
        pass2Field.send_keys('iceprincess47')

        # The he clicked signup
        signupButton = self.browser.find_element_by_id('signupButton')
        signupButton.click()
        time.sleep(2)

        # Damian notices a message popped up, he gets redirected to the login page
        # There he sees some fields similar to the previous ones
        unameField = self.browser.find_element_by_id('id_username')
        passField = self.browser.find_element_by_id('id_password')

        self.assertEqual(
            passField.get_attribute('name'),
            'password'
        )

        # He entered the username and password he used to signup earlier 
        unameField.send_keys('Putri_Badai')
        passField.send_keys('iceprincess47')

        # Damian clicked log in. 
        loginButton = self.browser.find_element_by_id('loginButton')
        loginButton.click()

        # Upon logging in, he sees Putri Badai's name on the page 
        # Exactly like the username he entered before 
        greetings = self.browser.find_element_by_id('greetings')
        self.assertIn('Putri_Badai', greetings.text)

        # CHALLENGE STORY10
        # Damian sees an edit profile button and decides to click it 
        edit = self.browser.find_element_by_id('editProfile')
        edit.click()

        # He enters her full name 
        fnameField = self.browser.find_element_by_id('fname')
        lnameField = self.browser.find_element_by_id('lname')

        fnameField.send_keys('Putri')
        lnameField.send_keys('Badai')

        # And entered an image url of her photo 
        imgField = self.browser.find_element_by_id('image')
        imgField.send_keys('https://image.freepik.com/free-vector/princess-fairytale-fantasy-avatar-character_24911-52521.jpg')

        # Damian clicked save 
        save = self.browser.find_element_by_id('saveEdit')
        save.click()

        # Now the profile page displays the new infos as well 
        fullname = self.browser.find_element_by_id('fullname')
        self.assertIn('Putri Badai', fullname.text)

        # Satisfied, Damian logs back out because seeing her name for too long gets him nervous
        logoutButton = self.browser.find_element_by_id('logoutButton')
        logoutButton.click()

        # Putri's name disappear as he logged out
        header = self.browser.find_element_by_class_name('header')
        self.assertNotIn('Putri_Badai', header.text)
        self.assertIn('Log out', header.text)

        # Damian remembered he had band practice, so he left the page and met up with his band mate
        # It was a good day