from django.test import TestCase
from django.urls import resolve, reverse
from .views import index

# Create your tests here.
class HomePageTest(TestCase):

    def test_root_url_resolves_to_index_view(self):
        found = resolve(reverse('story4:index'))
        self.assertEqual(found.func, index)
    
    def test_home_page_returns_correct_html(self):
        response = self.client.get(reverse('story4:index'))
        html = response.content.decode('utf8')
        self.assertIn('Profile', html)
        self.assertTemplateUsed(response, 'story4/index.html')